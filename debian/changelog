kongress (24.12.2-1) unstable; urgency=medium

  [ Aurélien COUDERC ]
  * Package moved to the KDE Gear set.
  * Add/update Heiko Becker’s key in upstream keyring.
  * New upstream release (24.12.2).
  * Build against Qt6, update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.7.0, no change required.
  * Switch to declarative KF6 debhelper build sequence.
  * Switch to dh-sequence-qmldeps for runtime QML dependencies.
  * Refresh upstream metadata.
  * Review copyright information.

 -- Aurélien COUDERC <coucouf@debian.org>  Thu, 13 Feb 2025 22:27:24 +0100

kongress (1.0.1-2) unstable; urgency=medium


  [ Aurélien COUDERC ]
  * Switch maintainer email address to the alioth-lists.debian.net
    domain.
  * Update build-deps and deps with the info from cmake.
  * Bump Standards-Version to 4.6.2, no change required.
  * Update upstream metadata.
  * Add lintian override for kongress not having a manpage.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 02 Jan 2023 23:51:47 +0100

kongress (1.0.1-1) unstable; urgency=medium

  * New upstream release (1.0.1):
    - Fix display of talk time on notifications
    - Prevent kongressac from crashing
    - Fix grouping of talks into conference days
    - Fix alarm schedule time
    - Prevent duplicate conference entries from being displayed
    - Do not display generic, multi-day calendar events on the daily schedule
    - Fix translation congiguration
  * Minor fixes to debian/copyright.

 -- Aurélien COUDERC <coucouf@debian.org>  Mon, 29 Mar 2021 10:42:07 +0200

kongress (1.0-1) unstable; urgency=medium

  * Initial release. (Closes: #981109)

 -- Aurélien COUDERC <coucouf@debian.org>  Tue, 26 Jan 2021 16:26:16 +0100
